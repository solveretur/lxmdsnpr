import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.41"
}

group = "com.lxmdsnpr"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

object Versions {
    const val ktlint = "0.36.0"
}

val ktlint: Configuration by configurations.creating

tasks.register<JavaExec>("ktlint") {
    group = "verification"
    description = "Check Kotlin code style."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args( "src/**/*.kt")
}

tasks.named("check") {
    dependsOn(ktlint)
}

tasks.register<JavaExec>("ktlintFormat") {
    group = "formatting"
    description = "Fix Kotlin code style deviations."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args( "-F", "src/**/*.kt")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    ktlint("com.pinterest:ktlint:${Versions.ktlint}")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
